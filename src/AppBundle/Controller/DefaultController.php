<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use AppBundle\Form\Type\SignupType;
use AppBundle\Form\Type\LoginType;
use AppBundle\Entity\DemoUser;

class DefaultController extends Controller
{
    /**
     * @Route("/app/example", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/signup", name="signup_route")
     */
    public function signupAction(Request $request)
    {
        $signupForm = $this->createForm(new SignupType());

        if ($request->isMethod(Request::METHOD_POST) &&
            $signupForm->handleRequest($request)->isValid()) {
            
            $formData = $signupForm->getData();
            
            $newUser = new DemoUser();
            $newUser->setEmail($formData['email']);
            $newUser->setName($formData['name']);
            $newUser->setPassword($formData['password']); // Hash this
            
            $violations = $this->get('validator')->validate($newUser);
            
            if (0 === count($violations)) {
                $this->getDoctrine()->getManager()->persist($newUser);
                $this->getDoctrine()->getManager()->flush();

                // we auto-login the new user by setting a security token in the session
                
                $securityToken = new UsernamePasswordToken(
                    $newUser,
                    $newUser->getPassword(),
                    'default',
                    $newUser->getRoles()
                );
                
                $this->get('security.token_storage')->setToken($securityToken);

                return new Response("Name: " . $this->get('security.token_storage')->getToken()->getUser()->getName(), Response::HTTP_OK);
            }
        }
        
        return $this->render(
            'unauth/signup.html.twig',
            [
                'signupForm' => $signupForm->createView()
            ],
            new Response('', 0 < count($signupForm->getErrors()) ? Response::HTTP_BAD_REQUEST : Response::HTTP_OK)
        );
    }
    
    /**
     * @Route("/login", name="login_route")
     */
    public function loginAction(Request $request)
    {
        $loginForm = $this->createForm(new LoginType);
        
        if ($request->isMethod(Request::METHOD_POST) &&
            $loginForm->handleRequest($request)->isValid()) {
            
            $formData = $loginForm->getData();
            
            $user = $this->get('user_repo')->findOneBy([
               'email' => $formData['email']
            ]);
            
            if ($user === null) {
                return new Response("Invalid Credentials.", Response::HTTP_BAD_REQUEST);
            }
            else {
                $securityToken = new UsernamePasswordToken(
                    $user,
                    $user->getPassword(),
                    'default',
                    $user->getRoles()
                );
                
                $this->get('security.token_storage')->setToken($securityToken);
                
                return new Response("Email: " . $this->get('security.token_storage')
                                                     ->getToken()
                                                     ->getUser()
                                                     ->getEmail(),
                                    Response::HTTP_OK
                );
            }
        }
        
        return $this->render(
            'unauth/login.html.twig',
            [
                'loginForm' => $loginForm->createView()
            ],
            new Response('', 0 < count($loginForm->getErrors()) ? Response::HTTP_BAD_REQUEST : Response::HTTP_OK)
        );
    }
    
    /**
     * @Route("/logout", name="logout_route")
     */
    public function logoutAction(Request $request)
    {
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();
        
        return $this->redirect($this->generateUrl('homepage'));
    }
}
