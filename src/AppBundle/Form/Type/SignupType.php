<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

use AppBundle\Entity\DemoUser;

/**
 * Sign up form type
 */
class SignupType extends AbstractType
{
    /**
     * {@inheritDoc}
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text', [
                    'constraints' => [
                        new Assert\NotBlank(['message' => 'Name must not be empty']),
                        new Assert\Length([
                            'min' => DemoUser::CONSTRAINT_NAME_LENGTH,
                            'minMessage' => 'Name must be at least ' . DemoUser::CONSTRAINT_NAME_LENGTH .' characters'
                        ])
                    ]
                ])
                ->add('email', 'email', [
                    'constraints' => [
                        new Assert\Email(['message' => 'Invalid email']),
                        new Assert\NotBlank(['message' => 'Email must not be empty'])
                    ]
                ])
                ->add('password', 'password', [
                    'constraints' => [
                        new Assert\NotBlank(['message' => 'Password must not be empty']),
                        new Assert\Length([
                            'min' => DemoUser::CONSTRAINT_PASSWORD_LENGTH,
                            'minMessage' => 'Password must be at least ' . DemoUser::CONSTRAINT_PASSWORD_LENGTH .' characters'
                        ])
                    ]
                ])
                ->add('check', 'hidden', [
                    'constraints' => [
                        new Assert\Blank()
                    ]
                ]);
    }
    
    /**
     * {@inheritDoc}
     * 
     * @return string
     */
    public function getName()
    {
        return 'signup';
    }
}
