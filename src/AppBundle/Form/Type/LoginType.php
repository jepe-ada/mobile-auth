<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

use AppBundle\Entity\DemoUser;

/**
 * Log in form type
 */
class LoginType extends AbstractType
{
    /**
     * {@inheritDoc}
     * 
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text')
                ->add('email', 'email')
                ->add('password', 'password')
                ->add('check', 'hidden', [
                    'constraints' => [
                        new Assert\Blank()
                    ]
                ]);
    }
    
    /**
     * {@inheritDoc}
     * 
     * @return string
     */
    public function getName()
    {
        return 'login';
    }
}
